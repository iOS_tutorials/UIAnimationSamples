//
//  AnimationCurvePicker.m
//  UIAnimationSamples
//
//  Created by Joan Barrull Ribalta on 27/04/16.
//  Copyright © 2016 com.giria. All rights reserved.
//

#import "AnimationCurvePicker.h"

@implementation AnimationCurvePicker

@synthesize animationlist;

+ (id) newAnimationCurvePicker:(id)pickerDelegate
{
    UINib *nib = [UINib nibWithNibName:@"AnimationCurvePicker" bundle:nil];
    NSArray *nibArray = [nib instantiateWithOwner:self options:nil];
    AnimationCurvePicker *me = [nibArray objectAtIndex: 0];
    me.animationlist.delegate = pickerDelegate;
    me.animationlist.dataSource = pickerDelegate;
    return me;
}

@end
