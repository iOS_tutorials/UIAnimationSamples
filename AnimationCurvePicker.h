//
//  AnimationCurvePicker.h
//  UIAnimationSamples
//
//  Created by Joan Barrull Ribalta on 27/04/16.
//  Copyright © 2016 com.giria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnimationCurvePicker : UIView

@property (nonatomic,weak) IBOutlet UITableView *animationlist;
+ (id) newAnimationCurvePicker:(id)pickDelegate;

@end
