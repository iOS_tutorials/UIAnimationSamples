//
//  FakeHUD.h
//  UIAnimationSamples
//
//  Created by Joan Barrull Ribalta on 27/04/16.
//  Copyright © 2016 com.giria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Animation.h"

@interface FakeHUD : UIView
- (IBAction)btnStop;
+ (id) newFakeHUD;
@end
