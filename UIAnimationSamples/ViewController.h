//
//  ViewController.h
//  UIAnimationSamples
//
//  Created by Joan Barrull Ribalta on 27/04/16.
//  Copyright © 2016 com.giria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Animation.h"
#import "AnimationCurvePicker.h"
#import "FakeHUD.h"

@interface ViewController : UIViewController {
    NSMutableArray *curvesList;
    int	selectedCurveIndex;
    UIView *pickerView;
}

@property (strong, nonatomic) IBOutlet UITextField *textField;

@property (nonatomic,weak) IBOutlet UIButton *movingButton;

- (IBAction) btnMoveTo:(id)sender;
- (IBAction) btnDownUnder:(id)sender;
- (IBAction) btnZoom:(id)sender;
- (IBAction) btnHUD:(id)sender;

@end

