//
//  ViewController.m
//  UIAnimationSamples
//
//  Created by Joan Barrull Ribalta on 27/04/16.
//  Copyright © 2016 com.giria. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

/// put this just under @synthesize...
static int curveValues[] = {
    UIViewAnimationOptionCurveEaseInOut,
    UIViewAnimationOptionCurveEaseIn,
    UIViewAnimationOptionCurveEaseOut,
    UIViewAnimationOptionCurveLinear };


- (void)viewDidLoad
{
    [super viewDidLoad];
    curvesList = @[@"EaseInOut",@"EaseIn",@"EaseOut",@"Linear"].mutableCopy;
    selectedCurveIndex = 0;
    [self shakeView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) btnMoveTo:(id)sender
{
    UIButton* button= (UIButton*)sender;
    [_movingButton moveTo:
     CGPointMake(button.center.x - (_movingButton.frame.size.width/2),
                 button.frame.origin.y - (_movingButton.frame.size.height + 5.0))
                duration:1.0
                  option:curveValues[selectedCurveIndex]];	// above the tapped button
}

- (IBAction) btnDownUnder:(id)sender
{
    UIButton* button= (UIButton*)sender;
    [button downUnder:1.0 option:curveValues[selectedCurveIndex]];
}
/// Then add the following lines:
- (IBAction) btnZoom:(id)sender
{
    UIButton* button= (UIButton*)sender;
    pickerView = [AnimationCurvePicker newAnimationCurvePicker:self];
    // the animation will start in the middle of the button
    pickerView.center = button.center;
    [self.view addSubviewWithZoomInAnimation:pickerView duration:1.0 option:curveValues[selectedCurveIndex]];
}

#pragma mark - animation curves picker
// handling the picker, that is a simple tableview in this example
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [curvesList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellID"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.textLabel.text = [curvesList objectAtIndex:indexPath.row];
    
    if (indexPath.row == selectedCurveIndex)
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Select the Animation Curve to be used";
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"Curves will not affect total duration but instant speed and acceleration";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    if (selectedCurveIndex != indexPath.row)
    {
        NSIndexPath *oldPath = [NSIndexPath indexPathForRow:selectedCurveIndex inSection:indexPath.section];
        cell = [tableView cellForRowAtIndexPath:oldPath];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        selectedCurveIndex = indexPath.row;
    }
    
    if (pickerView)
    {
        [pickerView removeWithZoomOutAnimation:1.0 option:curveValues[selectedCurveIndex]];
        pickerView = nil;
    }
}

- (IBAction) btnHUD:(id)sender
{
    FakeHUD *theSubView = [FakeHUD newFakeHUD];
    [self.view addSubviewWithFadeAnimation:theSubView duration:1.0 option:curveValues[selectedCurveIndex]];
}

-(void)shakeView {
    
    CABasicAnimation *shake = [CABasicAnimation animationWithKeyPath:@"position"];
    [shake setDuration:0.1];
    [shake setRepeatCount:10];
    [shake setAutoreverses:YES];
    [shake setFromValue:[NSValue valueWithCGPoint:
                         CGPointMake(_textField.center.x - 5,_textField.center.y)]];
    [shake setToValue:[NSValue valueWithCGPoint:
                       CGPointMake(_textField.center.x + 5, _textField.center.y)]];
    [_textField.layer addAnimation:shake forKey:@"position"];
}


@end
